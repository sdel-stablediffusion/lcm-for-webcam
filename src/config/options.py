from argparse import ArgumentParser
from dataclasses import dataclass
from typing import Union
from yaml import safe_load


@dataclass(frozen=True)
class CameraOptions:
    id: int
    fps: Union[float, None]
    width: Union[int, None]
    height: Union[int, None]


@dataclass(frozen=True)
class GenerateOptions:
  seed: Union[int, None]
  cfg: Union[float, None]
  denoise: Union[float, None]
  steps: Union[int, None]
  prompt: Union[str, None]


@dataclass(frozen=True)
class Options():
    """ 各オプションを保持するクラス

    バリデーション等は行わない
    """
    lcm_model_name: str
    generation_model_name: str
    vae_path: Union[str, None]
    lora_path: Union[str, None]
    lora_strength: float
    camera: CameraOptions
    generation: GenerateOptions

    # ToDo:
    #     以下の `parse_args()`と `read_config_file()` は、
    #     個別の関数で定義せず、このクラスのメソッドとして持たせた方が便利そう。


def parse_args() -> Options:
    """ コマンドライン引数をパースし、Options にまとめて返す。"""
    parser = ArgumentParser()
    # default= が指定されていないオプションの初期値は None
    parser.add_argument("-lmn", "--lcm_model_name", type=str)
    parser.add_argument("-gmn", "--generation_model_name", type=str)
    parser.add_argument("-vp", "--vae_path", type=str)
    parser.add_argument("-lp", "--lora_path", type=str)
    parser.add_argument("-ls", "--lora_strength", type=float, default=1.0)
    parser.add_argument("-ci", "--camera_id", type=int, default=0)
    parser.add_argument("-cf", "--camera_fps", type=float)
    parser.add_argument("-cw", "--camera_width", type=int)
    parser.add_argument("-ch", "--camera_height", type=int)
    parser.add_argument("-gse", "--generate_seed", type=int)
    parser.add_argument("-gc", "--generate_cfg", type=float)
    parser.add_argument("-gd", "--generate_denoise", type=float)
    parser.add_argument("-gst", "--generate_steps", type=int)
    parser.add_argument("-gp", "--generate_prompt", type=str)
    args = parser.parse_args()

    return Options(
        args.lcm_model_name,
        args.generation_model_name,
        args.vae_path,
        args.lora_path,
        args.lora_strength,
        CameraOptions(
            args.camera_id,
            args.camera_fps,
            args.camera_width,
            args.camera_height
        ),
        GenerateOptions(
            args.generate_seed,
            args.generate_cfg,
            args.generate_denoise,
            args.generate_steps,
            args.generate_prompt
        )
    )


def read_config_file() -> Options:
    """ 設定ファイル (config.yaml) を読み取り、Options に格納して返す。"""
    with open("config.yaml", "r") as file:
        data = safe_load(file)
        return Options(
            data["lcm_model_name"],
            data["generation_model_name"],
            data["vae_path"],
            data["lora_path"],
            data["lora_strength"],
            CameraOptions(
                data["camera"]["id"],
                data["camera"]["fps"],
                data["camera"]["width"],
                data["camera"]["height"]
            ),
            GenerateOptions(
                data["generate"]["seed"],
                data["generate"]["cfg"],
                data["generate"]["denoise"],
                data["generate"]["steps"],
                data["generate"]["prompt"]
            )
        )

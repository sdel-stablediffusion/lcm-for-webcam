from typing import Union
from . import Options, parse_args, read_config_file


class CameraConfig:
    def __init__(
        self,
        id: int,
        fps: Union[float, None],
        width: Union[int, None],
        height: Union[int, None]
    ):
        self.id = id
        self.fps = fps
        self.width = width
        self.height = height

        if self.fps and self.fps <= 0:
            self.fps = None
        if self.width and self.width <= 0:
            self.width = None
        if self.height and self.height <= 0:
            self.height = None


class GenerationConfig:
    def __init__(
        self,
        seed: Union[int, None],
        cfg: Union[float, None],
        denoise: Union[float, None],
        steps: Union[int, None],
        prompt: Union[str, None]
    ):
        self.seed = seed
        self.cfg = cfg
        self.denoise = denoise
        self.steps = steps
        self.prompt = prompt

        if not self.seed or self.seed < 0:
            self.seed = 0
        if not self.cfg or self.cfg < 0:
            self.cfg = 0.0
        if not self.denoise or self.denoise < 0:
            self.denoise = 0
        if not self.steps or self.steps < 1:
            self.steps = 1
        if not self.prompt:
            self.prompt = ""


class Config():
    """ユーザー設定を保持するクラス

    ユーザー設定はコマンドライン引数、
    設定ファイル (config.yaml) の両方から受付け、
    コマンドライン引数の入力があればそちらを優先する。
    また、各値のバリデーションもこのクラスで行う。
    """
    def __init__(self, args: Options, config_file: Options):
        self.lcm_model_name = (
            args.lcm_model_name
            or config_file.lcm_model_name
        )
        self.generation_model_name = (
            args.generation_model_name
            or config_file.generation_model_name
        )
        self.vae_path = args.vae_path or config_file.vae_path
        self.lora_path = args.lora_path or config_file.lora_path
        self.lora_strength = args.lora_strength or config_file.lora_strength
        self.camera = CameraConfig(
            args.camera.id or config_file.camera.id,
            args.camera.fps or config_file.camera.fps,
            args.camera.width or config_file.camera.width,
            args.camera.height or config_file.camera.height
        )
        self.generation = GenerationConfig(
            args.generation.seed or config_file.generation.seed,
            args.generation.cfg or config_file.generation.cfg,
            args.generation.denoise or config_file.generation.denoise,
            args.generation.steps or config_file.generation.steps,
            args.generation.prompt or config_file.generation.prompt
        )

        if not self.lcm_model_name:
            raise ValueError("LCM モデルの指定が無い")
        if not self.generation_model_name:
            raise ValueError("生成モデルの指定が無い")


def load_config() -> Config:
    return Config(
        parse_args(),
        read_config_file()
    )

from cv2 import (
    VideoCapture,
    CAP_DSHOW,
    CAP_PROP_FPS,
    CAP_PROP_FRAME_WIDTH,
    CAP_PROP_FRAME_HEIGHT,
#   CAP_PROP_SETTINGS, <- カメラの詳細設定を開けるが不要なのでコメントアウト
    imshow,
    waitKey
)
from cv2.typing import MatLike

from config import CameraConfig
from utils import retrieve_valid_webcam_ids


class CameraNotFoundError(Exception):
    def __init__(
            self,
            message: str = "有効なカメラが見つからなかった"
        ):
        self.message = message


class Camera:
    def __init__(self, config: CameraConfig):
        camera_ids = retrieve_valid_webcam_ids()
        if len(camera_ids) == 0:
            raise CameraNotFoundError()

        self.camera_id = config.id or camera_ids[0] or 0
        self.camera = VideoCapture(self.camera_id, CAP_DSHOW)

        if config.fps:
            self.camera.set(CAP_PROP_FPS, config.fps)
        if config.width:
            self.camera.set(CAP_PROP_FRAME_WIDTH, config.width)
        if config.height:
            self.camera.set(CAP_PROP_FRAME_HEIGHT, config.height)

    def preview(self):
        try:
            while self.camera.isOpened():
                success, frame = self.camera.read()
                if not success:
                    continue

                imshow(
                    f"Camera[{self.camera_id}] (Press Q key to Quit Preview)",
                    frame
                )
                if waitKey(1) & 0xFF == ord("q"):
                    break
        except KeyboardInterrupt:
            self.camera.release()

    def read_frame(self) -> tuple[bool, MatLike]:
        if not self.camera.isOpened():
            raise IOError()
        waitKey(1)
        return self.camera.read()

    def release(self):
        self.camera.release()

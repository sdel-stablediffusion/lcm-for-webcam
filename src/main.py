from cv2 import COLOR_BGR2RGB, COLOR_RGB2BGR, cvtColor, imshow
from diffusers.loaders.lora import LoraLoaderMixin
from diffusers.models.autoencoders.autoencoder_kl import AutoencoderKL
from diffusers.pipelines.auto_pipeline import AutoPipelineForImage2Image
from diffusers.pipelines.stable_diffusion.pipeline_stable_diffusion_img2img import StableDiffusionImg2ImgPipeline
from diffusers.schedulers.scheduling_lcm import LCMScheduler
from numpy import array
from PIL.Image import fromarray
from torch import float16, Generator

from config import Config, load_config
from camera import Camera


def create_pipeline(config: Config) -> StableDiffusionImg2ImgPipeline:
    if config.vae_path:
        pipeline = AutoPipelineForImage2Image.from_pretrained(
            config.generation_model_name,
            torch_dtype=float16,
            use_safetensors=True,
            vae=AutoencoderKL.from_single_file(
                config.vae_path,
                torch_dtype=float16
            )
        ).to("cuda")
    else:
        pipeline = AutoPipelineForImage2Image.from_pretrained(
            config.generation_model_name,
            torch_dtype=float16,
            use_safetensors=True
        ).to("cuda")

    if not isinstance(pipeline, StableDiffusionImg2ImgPipeline):
        raise TypeError("型が不正: StableDiffusionImg2ImgPipeline ではない")

    if not issubclass(type(pipeline), LoraLoaderMixin):
        raise TypeError("型が不正: LoraLoderMixin のサブクラスではない")

    if config.lora_path:
        pipeline.load_lora_weights(config.lora_path, adapter_name="LoRA")
        pipeline.load_lora_weights(config.lcm_model_name, adapter_name="lcm")
        pipeline.set_adapters(
            ["LoRA", "lcm"],
            adapter_weights=[config.lora_strength, 1.0]
        )
    else:
        pipeline.load_lora_weights(config.lcm_model_name, adapter_name="lcm")

    pipeline.scheduler = LCMScheduler.from_config(pipeline.scheduler.config)

    if pipeline.safety_checker is not None:
        pipeline.safety_checker = lambda images, **kwargs: (images, [False])

    return pipeline


def main():
    config = load_config()
    pipeline = create_pipeline(config)
    generator = Generator("cuda").manual_seed(config.generation.seed or 0)
    camera = Camera(config.camera)

    try:
        while True:
            success, frame = camera.read_frame()
            if not success:
                continue

            img_pil = pipeline(
                prompt=config.generation.prompt or "",
                strength=config.generation.denoise or 0.4,
                image=fromarray(cvtColor(frame, COLOR_BGR2RGB)),
                num_inference_steps=config.generation.steps,
                guidance_scale=config.generation.cfg,
                generator=generator
            ).images[0]

            img = cvtColor(array(img_pil), COLOR_RGB2BGR)
            imshow(
                ", ".join([
                    f"Seed: { config.generation.seed }",
                    f"Denoise: { config.generation.denoise }",
                    f"Steps: { config.generation.steps }"
                ]),
                img
            )
    except KeyboardInterrupt:
        camera.release()

    exit(0)


if __name__ == "__main__":
    main()

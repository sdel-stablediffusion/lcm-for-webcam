from cv2 import VideoCapture, getBuildInformation


def get_build_information():
    print(getBuildInformation)


def retrieve_valid_webcam_ids(max_id: int=5) -> list[int]:
    id_list = []
    for id in range(-1, max_id):
        ret, _ = VideoCapture(id).read()
        if ret:
            id_list.append(id)

    return id_list
